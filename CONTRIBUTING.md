# Contributing to BrainDot

Guides on how to contribute to BrainDot projects can be found in the [contrib](contrib/) directory.
