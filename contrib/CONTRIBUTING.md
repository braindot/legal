# Contributing to BrainDot projects

This file presents the different steps needed to contribute to a BrainDot project.

## Get started

### Legal agreement

By submitting anything to a BrainDot repository, you agree to the terms of the Developer Certificate of Origin:

    Developer Certificate of Origin
    Version 1.1

    Copyright (C) 2004, 2006 The Linux Foundation and its contributors.
    1 Letterman Drive
    Suite D4700
    San Francisco, CA, 94129

    Everyone is permitted to copy and distribute verbatim copies of this
    license document, but changing it is not allowed.


    Developer's Certificate of Origin 1.1

    By making a contribution to this project, I certify that:

    (a) The contribution was created in whole or in part by me and I
        have the right to submit it under the open source license
        indicated in the file; or

    (b) The contribution is based upon previous work that, to the best
        of my knowledge, is covered under an appropriate open source
        license and I have the right under that license to submit that
        work with modifications, whether created in whole or in part
        by me, under the same open source license (unless I am
        permitted to submit under a different license), as indicated
        in the file; or

    (c) The contribution was provided directly to me by some other
        person who certified (a), (b) or (c) and I have not modified
        it.

    (d) I understand and agree that this project and the contribution
        are public and that a record of the contribution (including all
        personal information I submit with it, including my sign-off) is
        maintained indefinitely and may be redistributed consistent with
        this project or the open source license(s) involved.

You also agree to every license specified by the project (in the file LICENSE and/or in the header of project files), and to sign your commits.

Note that each project can replace this agreement with a different one, or add new requirements. In that case, the mention will appear either in the 'README.md', 'LICENSE', 'CONTRIBUTING.md' or other easy-to-find files. If you have any doubts, it is your responsibility to ask the maintainers of the project.

### Signing your commits

Signing commits is easy, and is required to push to BrainDot repositories. It allows your commits to be easily identifiable (it is easy to fake your identity in Git—this makes it impossible). It is your responsibility to keep the private key secret.

First, create a PGP key using GPG (can be easily installed using your favorite package manager).

    gpg --full-gen-key
    # or 'gpg --gen-key' on some Windows/MacOS versions

GPG will now prompt you for different information. BrainDot doesn't have any particular requirements, you can input what you want or keep the defaults.

Follow the [GitLab documentation](https://gitlab.com/help/user/project/repository/gpg_signed_commits/index.md) and add your GPG key to your GitLab account.

Add your GPG to Git so it knows which key to sign with:

    git config --global user.signingkey [your key ID here]
    git config --global commit.gpgsign true

You're all set! Your new commits should now appear as 'verified' on GitLab.

## What you will have to do for each project

When you start working on a new project, you will need to fork it.

To do this, go to the page of the project on GitLab, and click the 'fork' button on the top right. Select under which userspace it should be saved. If you already have a project named the same way, you should create a private group, fork the project to that private group, go into the Settings and change its URL, then fork it again, this time to your account, under the new name.

You should now clone the official project, rename the 'origin' remote for clarity, then add your fork as a remote. Here's an example for this project:

    git clone git@gitlab.com:braindot/legal.git
    cd legal
    git remote rename origin braindot
    git remote add [your name here] [your fork's URL]
    git fetch --all # test that everything is working well

You're now ready to contribute.

## What you will have to do for each contribution

Everytime you wish to submit a contribution, you will have to create a new branch.

    git checkout master
    git pull # Make sure you're on the latest version
    git checkout -b [name of your branch]

That branch represents that specific contribution. Write the code you want to add, and then do:

    git add --intent-to-add [new files you added, '.' to select everything]
    git add --path # Review and reread your changes, type '?' to get help on how to use this command
    git commit

Commits messages should follow the [BrainDot conventions](../coding-style/STYLE_Git.md). If you don't know which type or scope to
 use, your
 commit is probably too big and should be split in two. One commit should represent an abstract change: it's one 'step' of your work.

The first time you push your contribution, you will have to:

    git push --set-upstream [name of your fork] [name of your branch]

GitLab will then prompt you to create a Merge Request. Click the link and fill in the fields. If your fork is not direct (through a group to select another URL), check that you are submitting your Merge Request to the correct repository (in the top, 'target branch').

If your work is not done yet, or you think you still have things to change, start the Merge Request with 'WIP: ', this will tell GitLab that it shouldn't be merged yet. You can continue to commit to your branch. Use regular `git push` (no options) to update your Merge Request. Do not forget to remove the 'WIP: ' prefix when it's done.

During review, the following scenarii can occur:

**The reviewers are happy.** You don't have anything to do, they will merge the request themselves.

**GitLab tells you that your branch is not up-to-date and cannot be merged anymore.** To fix this, do (if you need help, [read this](https://git-scm.com/book/en/v2/Git-Branching-Rebasing)):

    git checkout [your branch]
    git pull --rebase=interactive braindot master
    git push -f

**The reviewers ask that something missing is added.** Add new commits in your branch, and push again.

**The reviewers ask that you fix/change something you have done.** Do this:

    git checkout [your branch]
    git rebase -i $(git merge-base HEAD master)
    # Select the commits you wish to change, edit them
    git push -f

Your submission will stay in Review until everything is alright. The maintainers will then merge it. That's it, you know how to contribute to every BrainDot project!
