# Contributing to BrainDot projects

This directory contains all information about submitting contributions on BrainDot projects.

## Summary

BrainDot projects are based on GitLab forks. Essentially, you will need to do once:
- Review the legal agreement
- Enable GPG signing in Git

Everytime you contribute to a new project, you will need to:
- Create a fork

Finally, for each contribution, you will need to:

- Create a feature branch
- Commit your changes
- Submit a Merge Request
- Understand code review

All those steps are explained in [the CONTRIBUTING.md file](CONTRIBUTING.md).

## Setup GitLab for BrainDot

See the [GitLab documentation](GITLAB.md).
