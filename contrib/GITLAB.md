# GitLab setup for BrainDot

## Project management

Issues » Labels:

- Create green labels `issue::soon` ("current sprint/milestone", "backlog"), `issue::doing` and `issue::review`.
- Create green labels `merge::draft`, `merge::developer` ("waiting for modifications") and `merge::reviewer` ("waiting for review")
- Create a purple label per module of your project (for example, `client`, `server`, `core`…)

Issues » Boards:

- Create columns `issue::soon`, `issue::doing` and `issue::review` (in this order).

Issues » Milestones:

- Create a milestone per sprint/objective/version you're going to go through. Do use start and end dates.

### Working with user stories

#### Within GitLab: requirements

Requirements:

- Create a requirement per user story
- When implementing it, create an issue called "Implement REQ-n" or similar

#### With another tool

Do not use issue tracking from another tool.

- When implementing a user story stored in another tool, create an issue "Implement user story `<name>`" and link to the other tool's user story. After that, work with milestones, issues etc as normal.

## Repository

Settings » General » Merge Requests:

- Merge method: merge commit
- Merge options:
	- Show link to create or view a merge request from the CLI
	- Enable "delete source branch" by default
- Squash commits: do not allow
- Merge checks:
	- Pipelines must succeed (only if this project has pipelines)
	- All discussions must be resolved

Settings » General » Merge request approvals

- Create an approval rule with the whole team, and the number of reviewers you want (recommended: 2)
- Checkboxes:
	- Require new approvals when new commits are added to an MR
	- Prevent approval by author and users who committed
- We recommend setting up [Code Owners](https://docs.gitlab.com/ee/user/project/code_owners.html), so everyone in the team can take advantage of their specialty

Settings » Repository » Protected branches:

- `master`: maintainers and developers are allowed to merge, no one is allowed to push, force push is forbidden, code owner approval is enabled.

### Additional settings if you're working with forks

Settings » Repository » Protected branches:

- `master` should be protected (see above)
- `*` should be protected (same rules as `master`)
