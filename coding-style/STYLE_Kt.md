# BrainDot Coding Style: Kotlin

This document inherits from [the main guide](STYLE.md). Please ensure you have read it.

## Naming

Variables and functions should be named following the `camelCase` convention.

The function name should reflect whether it mutates objects or not; for example use `sort` for in-place sorting, and `sorted` for copy sorting. If you override operators, follow the same conventions (`a + b` should mutate neither `a` nor `b`, `a += b` should mutate `a` but not `b`, etc).

Classes, objects, enums, etc, should be named following the `PascalCase` convention. Files should follow this convention as well. Variables that are used as objects (`val UserComparator = ...`) can be named following this convention as well.

Functions that take the role of a constructor (eg. factory methods) can be named after the class, with the same capitalization. See article 1 of Effective Java for the rationale.

When using acronyms of 2 letters, capitalize both. When using longer acronyms, only capitalize the first letter:

```kotlin
class IOException
class HttpInputStream
```

Constants (`const val`) should be named in `SCREAMING_SNAKE_CASE`.

Enum instances should be named in either `PascalCase` if they can be used as objects, and `SCREAMING_SNAKE_CASE` if they are used as values (settings, keys in a dictionary…). All instances in a particular enumeration should follow the same style.

When needing a private and a public property with the same name, it is allowed to name the private one with an underscore (the variable named with an underscore should never be available to the outside context):

```kotlin
class C {
    private val _elementList = mutableListOf<Element>()

    val elementList: List<Element>
         get() = _elementList
}
```

Prefer semantics over literal interpretation of these rules: if you have a `val` variable which holds an immutable object and is functionally equivalent to a `const val` declaration, name it as if it were one.

Some classes can have different implementations depending on whether they can be modified or not. In that case, follow the pattern used by the standard library (`List` and `MutableList`, `Set` and `MutableSet`, etc).

Do not use prefixes or suffixes like `ISomething`, `SomethingInterface` or similar for interfaces and classes in general. The `Impl` suffix (`FooImpl`) is allowed only if the class is private or otherwise inaccessible from outside the current context (private inner class, ...).

Test methods or other methods that are never manually called can be named with spaces (note that all Kotlin targets do not support this):

```kotlin
@Test
fun `test everything works well`() { ... }
```

If the backtick notation is not available on your platform, you can use underscores instead.

## Project structure

### Packages

Packages should be named following the Java conventions (website URL in reverse, followed by the project identifier), for instance:

    fr.braindot.documents

Do not use uppercase characters. Try not to use multiple words. If you really need to, just concatenate them: `org.example.somethingelse`.

### File organization

Group declarations based on what they do and are used for, not what they are.

Within a class declaration, organize contents as:

- Properties and `init` blocks
- Secondary constructors
- Method declarations
- Companion object

Do not sort your functions in any way: the aim is to have related declarations close together.

When inheriting, try to keep the inherited declarations in the same order they were originally in.

## Formatting

Follow the rules in the [the main guide](STYLE.md), with the following exceptions:

Do not use unnecessary semicolons.

Do not put spaces around the `rangeTo` operator (`0..5`).

Do not put spaces around the `::` operator.

Do not put spaces between type parameters:
```kotlin
class Test<Thing: Any?>
```

Do not put spaces around null operators, except the elvis operator:
```kotlin
println(person.name?.toUpperCase() ?: "Unknown")
println(person.name!!)
val foo: Person?
```

Put spaces around `{` and `}` in lambda syntax:
```kotlin
listOf(1, 2, 3)
    .map { it * it }
```

Put spaces around the inheritance operator:
```kotlin
class A : B
```

## Immutability

Use `val` instead of `var` whenever possible.

Hide properties whenever possible (all are valid, from best to worse):
```kotlin
private var theClientDoesntNeedThis = 0

var theClientOnlyNeedsToRead = 0
    private set

var theClientNeedsToWrite = 0
```
Consider overriding the setter to avoid corruption.

Prefer immutable classes to mutable classes whenever possible.

## Exceptions

Avoid using exceptions for anything that can be recovered from (network failure…). Instead, use nullable return types, or a construct like `Either` if you need to give the user the reason.

Use exceptions for programmation errors (impossible cases, validating parameters, division by zero…). Use `require` to validate parameters and `check` for post-conditions and internal tests.

## Interop

### With Java

If a Kotlin method expected to be called from Java throws a checked exception, it should declare it:
```kotlin
@Throws(IOException::class)
fun withException() = ...
```

If you have a property that you want to give Java access to, annotate it with `@JvmField`. Only do this for constant values.

If you have an object with a method you want to be available from Java without specifying the instance, annotate it with `@JvmStatic`.

For all methods with default parameters available from Java, consider adding the annotation `@JvmOverloads`.

### With JavaScript

Any function intended to be called from JS should have the `@JsName` annotation applied.
