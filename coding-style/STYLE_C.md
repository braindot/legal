# BrainDot Coding Style: C/C++

This document inherits from [the main guide](STYLE.md). Please ensure you have read it.

## Naming

Variables, functions, structures, etc, should be named following the `snake_case` convention.

Macros should be named in `ALL_UPPERCASES`, but macros that act as functions can be named in `snake_case` as well.

## Indentation

Indentation of `switch` statements should be done like this:

```c
switch (something) {
case 'A':
    printf("A");
    break;
case 'B':
    printf("B");
    break;
default:
    printf("?");
    break;
}
```

## Spaces

The general rules apply, however `sizeof`, `typeof`, `alignof` and `__attribute__` are handled like they were functions, even though they aren't (because they are used similarly):

```c
sizeof(struct file);
```

For pointer types, put the asterisk adjacent to the name and not to the type:

```c
char *something;
int *function(int *argument);
```

Structure-member operators (`.` and `->`) should not be surrounded by spaces:

```c
int a = data->value * 2 + (-3);
```

## Typedefs

In most cases, do not use typedefs.

If you think your structure will be used a lot, it is allowed to create a typedef for it, but not encouraged. If you choose to do so, the typedef should be on the following line of the structure declaration, and should be called the same way as the structure with an appended `_t`. For example, `struct something` corresponds to the typedef `something_t`.

Typedefs on pointer types are strictly prohibited.

You can also use typedefs for types that change depending on the implementation (are some time in some cases and some other type in some other cases).

## Functions

Functions that take no arguments should be declared with `void`:

```c
int something(void);
```

In headers, always specify the name of arguments:

```c
int something(int example);
```

## Exiting of functions

If your function is using dynamic allocation, prefer using `goto` statements rather than simple `return`s:

```c
{
    int result = 0;
    char *buffer;

    buffer = malloc(...);
    if (!buffer)
        return -ENOMEM;

    if (condition1) {
        while (loop1) {
            ...
        }
        result = 1;
        goto out_free_buffer;
    }
    ...
out_free_buffer:
    free(buffer);
    return result;
}
```

## Correctness

Any C code should always use `-Wall -pedantic` as compilation options (or an equivalent, for other compilers).

Any C code should pass a `valgrind` test with no errors and no warnings.

## Documentation

Use Doxygen comments of the shape:

```c
/**
 * General description of the function.
 * @param example Description of the parameter 'example'
 * @return Description of the result.
 */
int something(int example);
```

## Visibility

Everything in a header should be documented.

Do not declare in headers things that do not need to be publicly available.

Every function that is NOT declared in a header should be declared with the keyword `static`.

Things that are not declared in a header do not have to be documented as long as they are trivial to understand from a single look at their code. In other cases, do document them.

## Macros

Always check that your macro is not defined yet:

```c
#ifndef SOME_CONFIG
    #define SOME_CONFIG 2
#endif //SOME_CONFIG
```

This allows the macro to be passed as a compiler argument.

In the above example, the trailing comment `//SOME_CONFIG` can be omitted only if the macro is trivial (maximum of 4-5 lines) and well-indented.

Do not define macros with implicit values (such has `#define SOME_CONFIG`).
